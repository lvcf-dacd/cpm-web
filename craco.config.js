const CracoAntDesignPlugin = require('craco-antd');
const CracoAlias = require('craco-alias');
const { ESLINT_MODES } = require('@craco/craco');

module.exports = {
    babel: {
        plugins: [['@babel/plugin-proposal-decorators', { legacy: true }]],
    },
    plugins: [
        { plugin: CracoAntDesignPlugin },
        {
            plugin: CracoAlias,
            options: {
                source: 'tsconfig',
                baseUrl: './src',
                tsConfigPath: './tsconfig.extend.json',
            },
        },
    ],
    eslint: {
        mode: ESLINT_MODES.file,
    },
};
